$(".copy-clipboard").click(async function (event) {
  if (!navigator.clipboard) return;

  const text = event.target.ariaLabel;
  await navigator.clipboard.writeText(text);

  $(`#${event.target.id}`).attr("title", "Copied to Clipboard");

  $(`#${event.target.id}`).tooltip("show");
  setTimeout(function () {
    $(`#${event.target.id}`).tooltip("dispose");
  }, 1000);
});
